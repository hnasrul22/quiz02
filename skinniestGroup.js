const {getMostByGroup} = require('./utils/byGroup')
const data = require('./data')
const console = require('./utils/inspectFull')

const skinniestByGroup = data => getMostByGroup(data,'sort','weight','skinniest')

console(skinniestByGroup(data));