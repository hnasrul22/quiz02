const {getMostByGroup} = require('./utils/byGroup')
const data = require('./data')
const console = require('./utils/inspectFull')

const shortestByGroup = data => getMostByGroup(data,'sort','height','shortest')

console(shortestByGroup(data));