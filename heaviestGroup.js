const {getMostByGroup} = require('./utils/byGroup')
const data = require('./data')
const console = require('./utils/inspectFull')

const heaviestByGroup = data => getMostByGroup(data,'sort','weight','heaviest','desc')

console(heaviestByGroup(data));