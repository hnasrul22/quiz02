const data = require('./data');
const sortTallest = require('./utils/sort')

const getTallest = (data) => sortTallest(data,'height','desc')[0]

console.log(getTallest(data))