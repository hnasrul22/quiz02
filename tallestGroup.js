const {getMostByGroup} = require('./utils/byGroup')
const data = require('./data')
const console = require('./utils/inspectFull')

const tallestByGroup = data => getMostByGroup(data,'sort','height','tallest','desc')

console(tallestByGroup(data));