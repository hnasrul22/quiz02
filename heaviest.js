const data = require('./data')
const sort = require('./utils/sort')

const heaviest = (data) => sort(data,'weight','desc')[0]

console.log(heaviest(data));