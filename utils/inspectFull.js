const util = require('util');

const inspectFull = any => {
    console.log(util.inspect(any,{depth: null}));
}

module.exports = inspectFull