const sort = (data,by,on) => {
    return data.sort((a,b)=>{

        // if the value on is desc will make a descending sort
        if(on === 'desc'){
            return b[by] - a[by]
        }
        // sort by default or 'on' param is null will make an ascending sort
        return a[by] - b[by]
    })
}

module.exports = sort