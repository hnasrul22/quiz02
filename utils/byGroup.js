const getAverage = require("./getAverage");
const sort = require("./sort");

const getByGroup = (data) => {

    // Define inital array for new data
    let newObj = [];

    // Define initial array to collect all classname
    let classList = [];

    // Get all classname on object
    data.map(({class : className})=>{
        // classname can't double on array, so make a check logic
        if (!classList.includes(className)) classList.push(className)
    })

    // Push all data by filtering by classname
    classList.map(e => {

        newObj = [
            ...newObj,
            {
                class : e,
                participants : data.filter(({class : className}) => className === e)
            }
        ]

    })

    return newObj
}

const getMostByGroup = (data,type,by,label,on) =>{

    // Get all classname that available on object
    const getGroup = getByGroup(data)

    // Initial new obj to collect new data
    let newObj = []

    getGroup.map(({class : className, participants})=>{
        newObj = [
            ...newObj,
            {
                /**
                 * Create a new object
                 * - class will get from a classname
                 * - seconds key (label), it will dynamically from a param
                 * - there are 2 types available is 
                 *      * 'sort' that will call sort participant
                 *      * 'average' that will call getAverage participant
                 */
                class : className,
                [label] : type === 'sort' ? sort(participants,by,on)[0] : type === 'average' && getAverage(participants,by)
            }
        ]
    })

    return newObj
}




module.exports = {getByGroup,getMostByGroup}