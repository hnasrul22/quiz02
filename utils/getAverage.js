const getAverage = (data,by) => {
    
    // initial new array to collect all data needed
    let allValue = [];
    
    data.map((e)=>{

        // insert all 'by' value into an array
        allValue = [
            ...allValue,
            e[by]
        ]
    })

    // compute to get average value from array
    return (allValue.reduce((a,b)=>a+b)) / data.length
}

module.exports = getAverage