const participant = [
    {
      name: 'aji',
      class: '6B',
      height: 170,
      weight: 59
    },
    {
      name: 'Vian',
      class: '6A',
      height: 178,
      weight: 81
    },
    {
      name: 'Susi',
      class: '6A',
      height: 157,
      weight: 43
    },
    {
      name: 'Anang',
      class: '6C',
      height: 170,
      weight: 59
    },
    {
      name: 'Ari',
      class: '6B',
      height: 192,
      weight: 82
    },
    {
      name: 'Hafiz',
      class: '6B',
      height: 172,
      weight: 70
    },
    {
      name: 'Fauzi',
      class: '6C',
      height: 158,
      weight: 62
    },
    {
      name: 'Kaka',
      class: '6C',
      height: 170,
      weight: 59
    },
    {
      name: 'Luffy',
      class: '6B',
      height: 168,
      weight: 40
    },
    {
      name: 'Franky',
      class: '6A',
      height: 198,
      weight: 95
    }
   ]

   
module.exports = participant