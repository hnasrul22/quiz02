const data = require('./data')
const sort = require('./utils/sort')

const skinniest = (data) => sort(data,'weight')[0]

console.log(skinniest(data));